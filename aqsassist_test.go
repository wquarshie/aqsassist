package aqsassist

import "testing"

func TestValidateTimestamp(t *testing.T) {
	timestamp := "20220101"

	validatedTimestamp, err := ValidateTimestamp(timestamp)

	if len(validatedTimestamp) != 10 || err != nil {
		t.Fatalf(`Got '%s,', want 2022010100`, validatedTimestamp)
	}
}

func TestInvalidTimestamp(t *testing.T) {
	timestamp := ""

	_, err := ValidateTimestamp(timestamp)

	if err == nil {
		t.Fatalf(`Got %s, want parsing error`, err)
	}
}

func TestTrimProjectDomain(t *testing.T) {
	domain := "www.es.wikipedia.org"

	trimmedDomain := TrimProjectDomain(domain)

	if trimmedDomain != "es.wikipedia" {
		t.Fatalf(`Got %s, want es.wikipedia`, trimmedDomain)
	}
}

func TestFilterAgent(t *testing.T) {
	agent := "all-agents"

	total, _ := FilterAgent(agent, 1, 1)

	if total != 2 {
		t.Fatalf(`Got %d, want 2`, total)
	}
}

func TestBadFilterAgent(t *testing.T) {
	agent := "all"

	total, err := FilterAgent(agent, 1, 1)

	if total != 0 || err == nil {
		t.Fatalf(`Got %d, want to throw an error`, total)
	}
}

//ValidateDuration for monthly granularity
func TestFullMonth(t *testing.T) {
	start := "2022010100"
	end := "2022013100"
	granularity := "monthly"

	_, _, err := ValidateDuration(start, end, granularity)

	if err != nil {
		t.Fatalf(`Got %s, want nil`, err)
	}

}

func TestLessThanOneMonth(t *testing.T) {
	//Duration shorter than a month,
	//no full months in range
	start := "2022010100"
	end := "2022010200"
	granularity := "monthly"

	s, e, err := ValidateDuration(start, end, granularity)

	if err == nil {
		t.Fatalf("Got %s, %s, want to throw an err", s, e)
	}
}

func TestLessThanTwoMonths(t *testing.T) {
	//Duration longer than a month,
	//no full months in range
	start := "2022010200"
	end := "2022022700"
	granularity := "monthly"

	s, e, err := ValidateDuration(start, end, granularity)

	if err == nil {
		t.Fatalf("Got %s, %s, want to throw an err", s, e)
	}
}

//ValidateDuration for daily granularity

func TestDaily(t *testing.T) {
	//Duration shorter than a month,
	//no full months in range
	start := "2022010100"
	end := "2022010200"
	granularity := "daily"

	_, _, err := ValidateDuration(start, end, granularity)

	if err != nil {
		t.Fatalf("Got %s, want nil", err)
	}
}


func TestStartBeforeEnd(t *testing.T) {

	start := "2021020100"
	end := "2021010100"

	err := StartBeforeEnd(start, end)

	if err == nil {
		t.Fatalf("Err %s, want error", err)
	}

	err = StartBeforeEnd(start, start)

	if err != nil {
		t.Fatalf("Got %s, want nil", err)
	}
}

